package com.qg.qbase.qbap.client;

import com.github.nalukit.nalu.plugin.elemental2.client.NaluPluginElemental2;
import com.google.gwt.core.client.EntryPoint;
import org.dominokit.domino.rest.DominoRestConfig;
import org.dominokit.domino.ui.notifications.Notification;


public class QBAP implements EntryPoint {
  public void onModuleLoad() {
    QBAPApplication application = new QBAPApplicationImpl();
    application.run(new NaluPluginElemental2());
    DominoRestConfig.initDefaults();
    DominoRestConfig
            .getInstance()
            .setDefaultServiceRoot("http://127.0.0.1:8080/api/");
  }
}
