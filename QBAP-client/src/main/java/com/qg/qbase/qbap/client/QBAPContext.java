package com.qg.qbase.qbap.client;

import com.github.nalukit.nalu.client.context.IsContext;

public class QBAPContext implements IsContext {
  private boolean loggedIn;

  public QBAPContext() {
    // enter your constructor code here ...
  }

  public boolean isLoggedIn() {
    return this.loggedIn;
  }

  public void setLoggedIn(boolean loggedIn) {
    this.loggedIn = loggedIn;
  }
}
