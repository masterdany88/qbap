package com.qg.qbase.qbap.client;

import com.github.nalukit.nalu.client.application.AbstractApplicationLoader;
import com.github.nalukit.nalu.client.application.IsApplicationLoader;
import java.lang.Override;

public class QBAPLoader extends AbstractApplicationLoader<QBAPContext> {
  @Override
  public void load(IsApplicationLoader.FinishLoadCommand finishLoadCommand) {
    // enter your code here ...
    finishLoadCommand.finishLoading();
  }
}
