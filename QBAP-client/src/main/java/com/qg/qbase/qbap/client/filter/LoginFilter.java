package com.qg.qbase.qbap.client.filter;

import com.github.nalukit.nalu.client.filter.AbstractFilter;
import com.qg.qbase.qbap.client.QBAPContext;
import java.lang.Override;
import java.lang.String;


public class LoginFilter extends AbstractFilter<QBAPContext> {
  @Override
  public boolean filter(String route, String... parms) {
    if (!"/login/login".equals(route)) {
      if (!this.context.isLoggedIn()) {
        return false;
      }
    }
    return true;
  }

  @Override
  public String redirectTo() {
    return "/login/login";
  }

  @Override
  public String[] parameters() {
    return new String[]{};
  }
}
