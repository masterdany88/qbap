package com.qg.qbase.qbap.client.ui.application.content.home;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;
import java.lang.Override;

import org.dominokit.domino.ui.Typography.Paragraph;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;


public class HomeComponent extends AbstractComponent<IHomeComponent.Controller, HTMLElement> implements IHomeComponent {
  private Card card;

  public HomeComponent() {
    super();
  }

  @Override
  public void display(MyModel model) {
    card.appendChild(
            Row.create()
                    .addColumn(
                            Column.span12()
                                    .appendChild(Paragraph.create("Software version: 12345679"))
                    )
    );
    card.appendChild(
            Row.create()
                    .addColumn(
                            Column.span12()
                                    .appendChild(Paragraph.create("System date and time: Wed Mar 11 13:41:00 CET 2020"))
                    )
    );
    card.appendChild(
            Row.create()
                    .addColumn(
                            Column.span12()
                                    .appendChild(Paragraph.create("Q-Base connectivity: LAN"))
                    )
    );
  }

  @Override
  public void render() {
    card = Card.create("Welcome to Q-Base");
    initElement(card.element());
  }
}
