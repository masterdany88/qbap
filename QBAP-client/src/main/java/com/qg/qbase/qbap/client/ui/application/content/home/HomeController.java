package com.qg.qbase.qbap.client.ui.application.content.home;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.QBAPContext;
import com.qg.qbase.qbap.client.event.StatusChangeEvent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;
import java.lang.Override;


@Controller(
    route = "/application/home",
    selector = "content",
    componentInterface = IHomeComponent.class,
    component = HomeComponent.class
)
public class HomeController extends AbstractComponentController<QBAPContext, IHomeComponent, HTMLElement> implements IHomeComponent.Controller {
  private MyModel model;

  public HomeController() {
  }

  @Override
  public void start() {
    component.display(model);
    eventBus.fireEvent(new StatusChangeEvent("active screen: >>Home<<"));
  }
}
