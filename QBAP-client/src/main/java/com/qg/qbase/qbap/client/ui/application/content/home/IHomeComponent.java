package com.qg.qbase.qbap.client.ui.application.content.home;

import com.github.nalukit.nalu.client.component.IsComponent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;


public interface IHomeComponent extends IsComponent<IHomeComponent.Controller, HTMLElement> {
  void display(MyModel model);

  interface Controller extends IsComponent.Controller {
  }
}
