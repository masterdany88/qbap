package com.qg.qbase.qbap.client.ui.application.content.instrumentadministration;

import com.github.nalukit.nalu.client.component.IsComponent;
import com.qg.qbase.qbap.shared.model.InstrumentDto;
import elemental2.dom.HTMLElement;

import java.util.List;


public interface IInstrumentAdministrationComponent extends IsComponent<IInstrumentAdministrationComponent.Controller, HTMLElement> {
  void edit(List<InstrumentDto> instruments);

  interface Controller extends IsComponent.Controller {
      void updateInstrumentStatus(String id, Boolean value);
  }
}
