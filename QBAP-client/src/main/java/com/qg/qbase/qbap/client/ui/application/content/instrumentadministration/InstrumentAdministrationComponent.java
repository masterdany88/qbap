package com.qg.qbase.qbap.client.ui.application.content.instrumentadministration;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import com.qg.qbase.qbap.shared.model.InstrumentDto;
import elemental2.dom.HTMLDivElement;
import elemental2.dom.HTMLElement;
import elemental2.dom.Node;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.forms.SwitchButton;
import org.dominokit.domino.ui.grid.flex.FlexItem;
import org.dominokit.domino.ui.grid.flex.FlexLayout;
import org.dominokit.domino.ui.grid.flex.FlexWrap;
import org.dominokit.domino.ui.header.BlockHeader;
import org.dominokit.domino.ui.style.Color;
import org.jboss.elemento.Elements;

import java.util.List;

import static org.jboss.elemento.Elements.img;

public class InstrumentAdministrationComponent extends AbstractComponent<IInstrumentAdministrationComponent.Controller, HTMLElement> implements IInstrumentAdministrationComponent {
    private HTMLDivElement element = Elements.div().element();

    public InstrumentAdministrationComponent() {
        super();
    }

    @Override
    public void edit(List<InstrumentDto> instruments) {
//    element.appendChild(
//            Row.create()
//                    .addColumn(
//                            Column.span4()
//                                    .appendChild(buildInstrumentCard())).element());
        FlexLayout flexLayout = FlexLayout.create();
        flexLayout.setWrap(FlexWrap.WRAP_TOP_TO_BOTTOM);
        instruments.forEach(
                i -> flexLayout.appendChild(FlexItem.create().appendChild(buildInstrumentCard(i.getId(), i.isOnline())))
        );
        element.appendChild(flexLayout.element());
    }

    private Node buildInstrumentCard(String id, boolean online) {
        return Card.create("Id: " + id, "Online: " + online)
                .appendChild(img("img/QIAcube-qg.svg"))
                .appendChild(buildCheckButton(id, online)).element();
    }

    private SwitchButton buildCheckButton(String id, boolean online) {
        SwitchButton button = SwitchButton.create()
                .value(online)
                .setColor(Color.INDIGO);
        button.addChangeHandler(value -> getController().updateInstrumentStatus(id, value));
//        button.addChangeHandler(v -> Notification.createSuccess("e " + v).show());
        button.pauseChangeHandlers();
        return button;
    }

    @Override
    public void render() {
        element.appendChild(BlockHeader.create("Instrument Administration", "").element());
        initElement(element);
    }
}
