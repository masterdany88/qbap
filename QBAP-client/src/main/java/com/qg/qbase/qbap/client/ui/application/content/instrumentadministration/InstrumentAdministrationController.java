package com.qg.qbase.qbap.client.ui.application.content.instrumentadministration;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.InstrumentResourceFactory;
import com.qg.qbase.qbap.client.QBAPContext;
import com.qg.qbase.qbap.client.event.StatusChangeEvent;
import com.qg.qbase.qbap.shared.model.InstrumentDto;
import elemental2.dom.HTMLElement;
import org.dominokit.domino.ui.notifications.Notification;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


@Controller(
        route = "/application/instrumentAdministration",
        selector = "content",
        componentInterface = IInstrumentAdministrationComponent.class,
        component = InstrumentAdministrationComponent.class
)
public class InstrumentAdministrationController extends AbstractComponentController<QBAPContext, IInstrumentAdministrationComponent, HTMLElement> implements IInstrumentAdministrationComponent.Controller {
    private List<InstrumentDto> instruments;

    public InstrumentAdministrationController() {
    }

    @Override
    public void start() {
        instruments = IntStream.rangeClosed(1, 9)
                .mapToObj(i -> new InstrumentDto("sim000" + i, setStatus(i)))
                .collect(Collectors.toList());

        component.edit(instruments);
        eventBus.fireEvent(new StatusChangeEvent("active screen: >>InstrumentAdministration<<"));
        InstrumentResourceFactory.INSTANCE
                .getAll()
                .onSuccess(s -> Notification.createSuccess("SUCCESS"))
                .onFailed(f -> Notification.createDanger("FAIL"))
                .send();
    }

    private boolean setStatus(int i) {
        return (i % 2 == 0);
    }

    @Override
    public void updateInstrumentStatus(String id, Boolean online) {
        InstrumentResourceFactory.INSTANCE
                .getAll()
                .onSuccess(s -> Notification.createSuccess("SUCCESS"))
                .onFailed(f -> Notification.createDanger("FAIL"))
                .send();
        Notification.createSuccess("SS").show();
        InstrumentResourceFactory.INSTANCE
                .updateInstrumentStatus(id, online)
                .onSuccess(s -> Notification.createSuccess(id + " status updated to " + online).show())
                .onFailed(s -> Notification.createDanger("Failed to update " + id).show())
                .send();
    }
}
