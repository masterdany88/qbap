package com.qg.qbase.qbap.client.ui.application.content.qiasphereregistration;

import com.github.nalukit.nalu.client.component.IsComponent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;


public interface IQIAsphereRegistrationComponent extends IsComponent<IQIAsphereRegistrationComponent.Controller, HTMLElement> {
  void edit(MyModel model);

  interface Controller extends IsComponent.Controller {
  }
}
