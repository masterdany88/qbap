package com.qg.qbase.qbap.client.ui.application.content.qiasphereregistration;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import org.dominokit.domino.ui.cards.Card;


public class QIAsphereRegistrationComponent extends AbstractComponent<IQIAsphereRegistrationComponent.Controller, HTMLElement> implements IQIAsphereRegistrationComponent {
  private Card card;

  public QIAsphereRegistrationComponent() {
    super();
  }

  @Override
  public void edit(MyModel model) {
    // That's a good place to move your data out of the model into the widgets.
    // 
    // Using GWT 2.x you can use the editor framework and in this case
    // it is a good idea to edit and flush the data inside the presenter.
    card.setTitle(model.getActiveScreen());
  }

  @Override
  public void render() {
    card = Card.create("");
    initElement(card.element());
  }
}
