package com.qg.qbase.qbap.client.ui.application.content.qiasphereregistration;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.QBAPContext;
import com.qg.qbase.qbap.client.event.StatusChangeEvent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;
import java.lang.Override;


@Controller(
    route = "/application/qiaSphereRegistration",
    selector = "content",
    componentInterface = IQIAsphereRegistrationComponent.class,
    component = QIAsphereRegistrationComponent.class
)
public class QIAsphereRegistrationController extends AbstractComponentController<QBAPContext, IQIAsphereRegistrationComponent, HTMLElement> implements IQIAsphereRegistrationComponent.Controller {
  private MyModel model;

  public QIAsphereRegistrationController() {
  }

  @Override
  public void start() {
    // Here we simulate the creation of a model.
    // In the real world we would do a server call or
    // something else to get the data.
    model = new MyModel("This value is set using the edit method! The value is >>" + "QIAsphereRegistration" + "<<");
    // 
    // now, move the data out of the model into the widgets - that's what we do next
    component.edit(model);
    // update the statusbar at the bottom of the screen
    eventBus.fireEvent(new StatusChangeEvent("active screen: >>QIAsphereRegistration<<"));
  }
}
