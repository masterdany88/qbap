package com.qg.qbase.qbap.client.ui.application.content.signout;

import com.github.nalukit.nalu.client.component.IsComponent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;


public interface ISignOutComponent extends IsComponent<ISignOutComponent.Controller, HTMLElement> {
  void edit(MyModel model);

  boolean isDirty();

  boolean isValid();

  void flush(MyModel model);

  interface Controller extends IsComponent.Controller {
  }
}
