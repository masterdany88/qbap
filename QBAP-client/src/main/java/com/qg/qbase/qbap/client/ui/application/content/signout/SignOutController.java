package com.qg.qbase.qbap.client.ui.application.content.signout;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.QBAPContext;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;
import java.lang.Override;


@Controller(
    route = "/application/signOut",
    selector = "content",
    componentInterface = ISignOutComponent.class,
    component = SignOutComponent.class
)
public class SignOutController extends AbstractComponentController<QBAPContext, ISignOutComponent, HTMLElement> implements ISignOutComponent.Controller {
  private MyModel model;

  public SignOutController() {
  }

  @Override
  public void start() {
    context.setLoggedIn(false);
  }

}
