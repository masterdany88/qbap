package com.qg.qbase.qbap.client.ui.application.content.users;

import com.github.nalukit.nalu.client.component.IsComponent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;


public interface IUsersComponent extends IsComponent<IUsersComponent.Controller, HTMLElement> {
  void edit(MyModel model);

  interface Controller extends IsComponent.Controller {
  }
}
