package com.qg.qbase.qbap.client.ui.application.content.users;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.QBAPContext;
import com.qg.qbase.qbap.client.event.StatusChangeEvent;
import com.qg.qbase.qbap.shared.model.MyModel;
import elemental2.dom.HTMLElement;
import java.lang.Override;


@Controller(
    route = "/application/users",
    selector = "content",
    componentInterface = IUsersComponent.class,
    component = UsersComponent.class
)
public class UsersController extends AbstractComponentController<QBAPContext, IUsersComponent, HTMLElement> implements IUsersComponent.Controller {
  private MyModel model;

  public UsersController() {
  }

  @Override
  public void start() {
    // Here we simulate the creation of a model.
    // In the real world we would do a server call or
    // something else to get the data.
    model = new MyModel("This value is set using the edit method! The value is >>" + "Users" + "<<");
    // 
    // now, move the data out of the model into the widgets - that's what we do next
    component.edit(model);
    // update the statusbar at the bottom of the screen
    eventBus.fireEvent(new StatusChangeEvent("active screen: >>Users<<"));
  }
}
