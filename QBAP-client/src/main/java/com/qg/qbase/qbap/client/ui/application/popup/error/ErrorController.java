package com.qg.qbase.qbap.client.ui.application.popup.error;

import com.github.nalukit.nalu.client.component.AbstractErrorPopUpComponentController;
import com.github.nalukit.nalu.client.component.annotation.ErrorPopUpController;
import com.qg.qbase.qbap.client.QBAPContext;
import java.lang.Override;


@ErrorPopUpController(
    componentInterface = IErrorComponent.class,
    component = ErrorComponent.class
)
public class ErrorController extends AbstractErrorPopUpComponentController<QBAPContext, IErrorComponent> implements IErrorComponent.Controller {
  public ErrorController() {
  }

  @Override
  public void onBeforeShow() {
    this.component.clear();
  }

  @Override
  public void show() {
    this.component.edit(this.errorEventType, this.route, this.message, this.dataStore);
    this.component.show();
  }

  @Override
  public void doRouteHome() {
    this.router.route("/application/home");
  }
}
