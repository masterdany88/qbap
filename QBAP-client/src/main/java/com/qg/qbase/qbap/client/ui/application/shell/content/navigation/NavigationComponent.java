package com.qg.qbase.qbap.client.ui.application.shell.content.navigation;

import com.github.nalukit.nalu.client.component.AbstractComponent;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.tree.Tree;
import org.dominokit.domino.ui.tree.TreeItem;


public class NavigationComponent extends AbstractComponent<INavigationComponent.Controller, HTMLElement> implements INavigationComponent {
  private TreeItem HomeItem;

  private TreeItem InstrumentAdministrationItem;

  private TreeItem QIAsphereRegistrationItem;

  private TreeItem UsersItem;

  private TreeItem SignOutItem;

  public NavigationComponent() {
    super();
  }

  @Override
  public void render() {
    this.HomeItem = TreeItem.create("Home", Icons.ALL.home())
                    .addClickListener(e -> getController().doNavigateTo("home"));
    this.InstrumentAdministrationItem = TreeItem.create("InstrumentAdministration", Icons.ALL.airplay())
                    .addClickListener(e -> getController().doNavigateTo("instrumentAdministration"));
    this.QIAsphereRegistrationItem = TreeItem.create("QIAsphereRegistration", Icons.ALL.cloud())
                    .addClickListener(e -> getController().doNavigateTo("qiaSphereRegistration"));
    this.UsersItem = TreeItem.create("Users", Icons.ALL.perm_identity())
                    .addClickListener(e -> getController().doNavigateTo("users"));
    this.SignOutItem = TreeItem.create("SignOut", Icons.ALL.exit_to_app())
                    .addClickListener(e -> getController().doNavigateTo("signOut"));
    Tree tree = Tree.create("Navigation");
    tree.appendChild(HomeItem);
    tree.appendChild(InstrumentAdministrationItem);
    tree.appendChild(QIAsphereRegistrationItem);
    tree.appendChild(UsersItem);
    tree.appendChild(SignOutItem);
    initElement(tree.element());
  }
}
