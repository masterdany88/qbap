package com.qg.qbase.qbap.client.ui.application.shell.content.navigation;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.QBAPContext;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import java.lang.String;


@Controller(
    route = "/application/",
    selector = "navigation",
    componentInterface = INavigationComponent.class,
    component = NavigationComponent.class
)
public class NavigationController extends AbstractComponentController<QBAPContext, INavigationComponent, HTMLElement> implements INavigationComponent.Controller {
  public NavigationController() {
  }

  @Override
  public void doNavigateTo(String target) {
    switch (target) {
      case "home":
      router.route("/application/home");
      break;
      case "instrumentAdministration":
      router.route("/application/instrumentAdministration");
      break;
      case "qiaSphereRegistration":
      router.route("/application/qiaSphereRegistration");
      break;
      case "users":
      router.route("/application/users");
      break;
      case "signOut":
      context.setLoggedIn(false);
      router.route("application/home");
      break;
    }
  }
}
