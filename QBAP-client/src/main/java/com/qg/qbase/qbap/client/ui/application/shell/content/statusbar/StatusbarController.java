package com.qg.qbase.qbap.client.ui.application.shell.content.statusbar;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.QBAPContext;
import com.qg.qbase.qbap.client.event.StatusChangeEvent;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import org.gwtproject.event.shared.HandlerRegistration;


@Controller(
    route = "/application/",
    selector = "footer",
    componentInterface = IStatusbarComponent.class,
    component = StatusbarComponent.class
)
public class StatusbarController extends AbstractComponentController<QBAPContext, IStatusbarComponent, HTMLElement> implements IStatusbarComponent.Controller {
  private HandlerRegistration registration;

  public StatusbarController() {
  }

  @Override
  public void start() {
    this.registration = this.eventBus.addHandler(StatusChangeEvent.TYPE, e -> component.edit(e.getStatus()));
  }

  @Override
  public void stop() {
    this.registration.removeHandler();
  }
}
