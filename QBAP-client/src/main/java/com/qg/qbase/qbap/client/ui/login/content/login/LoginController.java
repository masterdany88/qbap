package com.qg.qbase.qbap.client.ui.login.content.login;

import com.github.nalukit.nalu.client.component.AbstractComponentController;
import com.github.nalukit.nalu.client.component.annotation.Controller;
import com.qg.qbase.qbap.client.QBAPContext;
import elemental2.dom.HTMLElement;
import java.lang.Override;
import java.lang.String;


@Controller(
    route = "/login/login",
    selector = "content",
    componentInterface = ILoginComponent.class,
    component = LoginComponent.class
)
public class LoginController extends AbstractComponentController<QBAPContext, ILoginComponent, HTMLElement> implements ILoginComponent.Controller {
  public LoginController() {
  }

  @Override
  public void doLogin(String userId, String password) {
    // we are always happy with the credential ...
    this.context.setLoggedIn(true);
    this.router.route("/application/home");
  }
}
