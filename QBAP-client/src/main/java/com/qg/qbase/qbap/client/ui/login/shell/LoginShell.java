package com.qg.qbase.qbap.client.ui.login.shell;

import com.github.nalukit.nalu.client.component.AbstractShell;
import com.github.nalukit.nalu.client.component.annotation.Shell;
import com.qg.qbase.qbap.client.QBAPContext;
import elemental2.dom.CSSProperties;
import java.lang.Override;
import org.dominokit.domino.ui.layout.Layout;
import org.dominokit.domino.ui.style.ColorScheme;


@Shell("login")
public class LoginShell extends AbstractShell<QBAPContext> {
  private Layout layout;

  public LoginShell() {
    super();
  }

  @Override
  public void attachShell() {
    layout = Layout.create("QBAP Q-Base Administration Portal")
                  .show(ColorScheme.INDIGO);
    layout.showFooter()
                  .fixFooter()
                  .getFooter()
                  .element().style.minHeight = CSSProperties.MinHeightUnionType.of("42px");
    layout.getFooter().setId("footer");
    layout.disableLeftPanel();
    layout.getContentPanel().setId("content");
    layout.getContentPanel().setWidth("50%");
  }

  @Override
  public void detachShell() {
    this.layout.remove();
  }
}
