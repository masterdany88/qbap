package com.qg.qbase.qbap.server;

import com.qg.qbase.qbap.shared.model.InstrumentDto;

import java.util.Arrays;
import java.util.List;

public class InstrumentController implements InstrumentResource {

    @Override
    public List<InstrumentDto> getAll() {
        System.out.println("Getting all instruments");
        return Arrays.asList(
                new InstrumentDto("sim1111", false),
                new InstrumentDto("sim2222", false),
                new InstrumentDto("sim3333", false)
        );
    }

    @Override
    public void updateInstrumentStatus(String id, boolean online) {
        System.out.println("Updating id = " + id + ", online = " + online);
    }

//    @Override
//    public void updateInstrumentStatus(String id, InstrumentDto instrumentDto) {
//        System.out.println("Updating id = " + id + ", instrumentDto = " + instrumentDto);
//    }
}
