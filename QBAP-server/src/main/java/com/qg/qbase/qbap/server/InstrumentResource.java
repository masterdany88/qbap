package com.qg.qbase.qbap.server;

import com.qg.qbase.qbap.shared.model.InstrumentDto;

import javax.ws.rs.*;
import java.util.List;

@Path("instruments")

@Produces("application/json")
@Consumes("application/json")
public interface InstrumentResource {

    @GET
    @Path("all")
    List<InstrumentDto> getAll();

    @Path("{id}/status")
    @PUT
//    void updateInstrumentStatus(@PathParam("id") String id, InstrumentDto instrumentDto);
    void updateInstrumentStatus(@PathParam("id") String id, boolean online);

}
