package com.qg.qbase.qbap.server;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("test")
@Produces("application/json")
@Consumes("application/json")
public class Test {
    @GET public String test() {
        return "succeed";
    }
}
