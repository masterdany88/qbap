package com.qg.qbase.qbap.shared.model;

public class InstrumentDto {
    private String id;
    private boolean online;

    public InstrumentDto() {
    }

    public InstrumentDto(String id, boolean online) {
        this.id = id;
        this.online = online;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isOnline() {
        return online;
    }

    public void setOnline(boolean online) {
        this.online = online;
    }
}
