package com.qg.qbase.qbap.shared.resources;

import com.qg.qbase.qbap.shared.model.InstrumentDto;

import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import java.util.Collection;

@Path("instruments")
public interface InstrumentResource {

    @GET
    Collection<InstrumentDto> getAll();

    @Path("{id}/status")
    @PATCH
//    void updateInstrumentStatus(@PathParam("id") String id, InstrumentDto instrumentDto);
    void updateInstrumentStatus(@PathParam("id") String id, boolean online);

}
